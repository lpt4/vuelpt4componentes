import Crud from './components/crud';
import Navbar from './components/navbar';
import Tabela from './components/tabela';
import Filho from './components/filho';

module.exports = {
  Crud,
  Navbar,
  Tabela,
  Filho
}
import Vue from 'vue'
import Raven from 'raven-js'

export const utilsCrud = {
    data() {
        return {}
    },
    methods: {
        mostrarNotificacao(obj) {
            this.$q.notify({
                message: obj.msg,
                timeout: obj.timeout!==undefined ? obj.timeout : 5000,
                type: obj.type!==undefined ? obj.type : 'negative',
                textColor: obj.textColor!==undefined ? obj.textColor : 'white',
                icon: obj.icon!==undefined ? obj.icon : 'warning',
                position: obj.position!==undefined ? obj.position : 'top',
                actions: [{icon:'close',noDismiss:true}]
            })
        },
        mostrarMsgErro(obj) {
            console.log("resposta",obj)
            var msgErro = ""
            try {
                if (obj!==undefined && obj.status!==200 && obj.ok!==true) {
                    if (obj.status===0) msgErro = this.$t('CONECTION_ERROR')+obj.url.split('api/')[0]
                    var error = obj.body.error!==undefined ? obj.body.error : obj.body
                    if (error!==undefined && error!=="") {
                        if (error.code!==undefined) {
                            if (['USERNAME_EMAIL_REQUIRED','LOGIN_FAILED','USER_BLOCKED','ORGANIZATION_BLOCKED'].indexOf(error.code)>-1) msgErro = this.$t(error.code)
                            else msgErro = (error.code!==undefined ? error.code+": " : "")+error.message       
                        }
                        else msgErro = error.name+": "+error.message
                    }
                    if (error!==undefined && ((obj.status===401 && ['Authorization Required','Invalid Access Token'].indexOf(error.message)>-1) || (obj.status===403 && error.message==='Access Denied'))) {
                        msgErro = this.$t('ERROR_AUTORIZATION')
                        this.$router.push("/logoff")
                    }
                }
            }
            catch(e) {
                if (obj!==undefined && obj.status!==200 && obj.ok!==true)
                    msgErro = this.$t('ERROR_SERVER')
            }
            if (msgErro!=='') this.mostrarNotificacao({msg:msgErro})
        },
        dateStrintToLong(d,tipo) {
            try {return d===undefined || d==='' ? '' : Vue.moment(d,tipo==='d' ? 'DD/MM/YYYY' : (tipo==='h' ? 'HH:mm:ss' : 'DD/MM/YYYY HH:mm:ss')).valueOf()} catch(e){return ''}
        },
        formatarDataHora(s) {
            try {return s===undefined || s==='' ? '' : Vue.moment(s).format('DD/MM/YYYY HH:mm:ss')} catch(e){return ''}
        },
        formatarData(s) {
            try {return s===undefined || s==='' ? '' : Vue.moment(s).format('DD/MM/YYYY')} catch(e){return ''}
        },
        formatarHora(s) {
            try {return s===undefined || s==='' ? '' : Vue.moment(s).format('HH:mm:ss')} catch(e){return ''}
        },
        formatarDataHoraArquivo(d) {
            try {return Vue.moment(d===undefined || d==='' ? new Date() : d).format('YYYYMMDD_HHmmss')} catch(e){return ''}
        },
        formatarArray(s,campo) {
            let r = ''
            try {
                if (!Array.isArray(s)) return ''
                for (let item of s) {
                    r += (r==='' ? '' : ', ')                    
                    if (campo!==undefined) {
                        let value = item
                        for (let item2 of campo.split('.')) {
                            value = value[item2]
                            if (value===undefined) {
                                value = ''
                                break
                            }
                        }
                        r += value
                    }
                    else r += item
                }
            }
            catch(e){console.log(e)}
            return r
        },
        formatarObject(o,campo) {
            return o[campo]
        },
        formatarNumerico(s,decimal) {
            if (s!==null && s!==undefined) return parseFloat(s).toFixed(decimal).replace('.',',')
            return s
        },
        formatarIdade(data) {
            var idade = ''
            try {
                if (data!==null && data!==undefined) {
                    var d1 = Vue.moment(data)
                    var d2 = Vue.moment()
                    var y = d2.diff(d1,'years')
                    d1.add(y,'years')
                    var m = d2.diff(d1,'months')
                    d1.add(m,'months')
                    var d = d2.diff(d1,'days')

                    idade = (y>0 ? y+' ano'+(y>1 ? 's' : '') : '')
                    idade += (m>0 ? ((idade==='' ? '' : (d>0 ? ', ' : ' e '))+m+(m>1 ? ' meses' : ' mês')) : '')
                    idade += (d>0 ? ((idade==='' ? '' : ' e ')+d+' dia'+(d>1 ? 's' : '')) : '')
                }
            }
            catch(e) {console.log(e)}
            return idade
        },
        testarLogin(obj,s) {
            var retorno = true
            try {
                if (s==='login') {
                    if (obj!==undefined && obj.id!==undefined && obj.id!=='' && (Vue.moment().valueOf()-Vue.moment(obj.created).valueOf())<(obj.ttl*1000)) {
                        Raven.setUserContext({
                            login: obj.user.email,
                            id: obj.user.id
                        })
                        retorno = false
                        this.abrirRota("/")
                    }
                }
                else if (s==='principal') {
                    if (obj===undefined || obj.id===undefined || obj.id==='') {
                        retorno = false
                        this.abrirRota("/login")
                    }
                    else if ((Vue.moment().valueOf()-Vue.moment(obj.created).valueOf())>=(obj.ttl*1000)) {
                        retorno = false
                        this.abrirRota("/logoff")
                    }
                }
            }
            catch(e) {console.log(e)}
            return retorno
        },
        arrayToStringPorCampo(obj,campo) {
            var s = ''
            try {
                if (obj!==null && obj!==undefined)
                    for (var i=0;i<obj.length;i++)
                        s += (s==='' ? '' : ', ')+obj[i][campo]
            }
            catch(e) {console.log(e)}
            return s
        },
        isNull(obj) {
            return (obj===undefined || obj===null || obj==='')
        },
        setTituloPagina(titulo,subtitulo,menu) {
            this.$store.state.dados['tituloPagina'] = titulo
            this.$store.state.dados['subtituloPagina'] = subtitulo
            this.$store.state.dados['mostrarMenu'] = menu
        },
        mensagemValidacao(field) {
            if (field!==undefined)
                for (var p in field.$params)
                    if (!field[p]) {
                        if (p==='minLength' || p==='minValue') return this.$t('validacaoCampo.'+p,{valor:field.$params[p].min})
                        else return this.$t('validacaoCampo.'+p)
                    }
        },
        abrirRota(s) {
            this.$router.push(s)
        },
        abrirUrl(s) {
            window.open(s,'_blank')
        },
        getNewObjectId() {
            var timestamp = Math.floor(new Date().valueOf()/1000).toString(16)
            var machine = this.$store.state.dados['mongoMachineId']
            if (machine===undefined) {
                machine = Math.floor(Math.random()*(16777216))
                this.$store.commit('dados/setDados',{storeState:'mongoMachineId',dados:machine})
            }
            machine = machine.toString(16)
            var pid = Math.floor(Math.random()*(65536)).toString(16)
            var increment = Math.floor(Math.random()*(16777216)).toString(16)
            return '00000000'.substr(0,8-timestamp.length)+timestamp+
                   '000000'.substr(0,6-machine.length)+machine+
                   '0000'.substr(0,4-pid.length)+pid+
                   '000000'.substr(0,6-increment.length)+increment
        },
        converterDadoSemTipoObject(obj) {
            var dado = {}
            for (var o in obj)
                if (typeof obj[o] !== "object")
                    dado[o] = obj[o]
            return dado
        }
    }
}
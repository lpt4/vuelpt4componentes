# lpt4-componentes
> Componentes LPT4 em Vue.js

- Crud (`extends`)
- Navbar (`import`)
- Tabela (`import`)
- Filho (`import`)
- Galeria (`import`)
- Imprimir (`import`)

```
# install
npm install lpt4-componentes --save
```

## Crud
> Componente para utilizar como `extends` que contém as chamadas das funções de um crud básico
```
export default {
    extends: Crud
}
```

> Variáveis que devem estar na classe que estende
```
export default {
    data() {
        return {
            dado: {},
            config: {
                salvarTipo: 'create ou update',
                entidade: 'nome da entidade',
                listar: 'nome da rota list', //usar somente se for diferente
                entidadeApi: 'nome da entidade da chamada do loopback',
                storeState: 'nome para o store'
            }
        }
    }
}
```

> Pode-se alterar os `actions` padrões do Store do Vue
```
export default {
    data() {
        return {
            config: {
                    storeCarregarLista: 'novo nome', //'carregarListaCrudPadrao'
                    storeCarregarListaImprimir: 'novo nome', //'carregarListaImprimirCrudPadrao'
                    storeCarregar: 'novo nome', //'carregarCrudPadrao'
                    storeSalvar: 'novo nome' //'salvarCrudPadrao'
                    storeExcluir: 'novo nome' //'excluirCrudPadrao'
                }
            }
        }
    }
}
```

> Pode-se adicionar `include` nas chamadas do Vue
```
export default {
    data() {
        return {
            config: {
                    include: ['tabela']
                }
            }
        }
    }
}
```

>Função `async afterPost(data)` que pode ser implementada. Será sempre chamada depois de salvar os dados.
```
export default {
    methods: {
        async afterPost(data) {
            
        }
    }
}
```

>Função `async beforeOpenDelete(data)` que pode ser implementada. Será sempre chamada antes de abrir o excluir os dados.
```
export default {
    methods: {
        async beforeOpenDelete(data) {
            this.excluirOk = true/false
        }
    }
}
```


>Função `async beforeDelete(data)` que pode ser implementada. Será sempre chamada antes de excluir os dados.
```
export default {
    methods: {
        async beforeDelete(data) {
            
        }
    }
}
```

>Função `async afterDelete(data)` que pode ser implementada. Será sempre chamada depois de excluir os dados.
```
export default {
    methods: {
        async afterDelete(data) {
            
        }
    }
}
```

>Função `async afterCarregarDado()` que pode ser implementada. Será sempre chamada depois de carregar os dados.
```
export default {
    methods: {
        async afterCarregarDado() {
            
        }
    }
}
```

## Navbar
> Componente utilizado como `import` que contém os botões de navegação
```
<navbar @executar="executar" :bListar="true" :bCriar="true" :bImprimir="true" :bExportarLabels="['CSV','JSON']" :bModalExcluirOpen="modalExcluirOpen"/>
```

| Funções | Parâmetros |
| ------ | ------ |
| @executar | executar (funcao do crud) |
| :bListar | true/false |
| :bCriar | true/false |
| :bEditar | true/false |
| :bExcluir | true/false |
| :bSalvar | true/false |
| :bCancelar | true/false |
| :bImprimir | true/false |
| :bExportarLabels | Array ['CSV','JSON'] |
| :bId | Object dado.id |
| :bModalExcluirOpen | modalExcluirOpen (variável do crud) |

>Função `async validar()` que pode ser implementada. Será sempre chamada antes de gravar os dados.
```
<navbar @executar="executar" :bSalvar="true" :bCancelar="true"/>
            
export default {
    methods: {
        async validar() {
            this.validado = false
        }
    }
}
```

| Slots |  |
| ------ | ------ |
| mais-botao | insere ao lado dos botoes atuais |

> Criando campo para `pesquisar`
```
<div class="row">
    <div class="col-md-12 q-pb-md">
        <q-field icon="search">
            <q-input v-model="pesquisarTexto" placeholder="Digite para pesquisar"/>
        </q-field>
    </div>
</div>

export default {
    data() {
        return {
            pesquisarTexto: '',
            pesquisarColunas: ['campo1','campo2',...]
        }
    }
}
```

## Tabela
> Componente utilizado como `import` que contém a tabela com os dados
```
<tabela :titulo="tabelaTitulo" :tabelaDados="tabelaDados" :tabelaColunas="tabelaColunas" :linhasPorPagina="linhasPorPagina" :tabelaPaginacao.sync="tabelaPaginacao" @executar="executar" @selecionarDado="selecionarDado" :tabelaLoading="tabelaLoading"/>

export default {
    data() {
        return {
            tabelaTitulo: 'Listagem',
            tabelaColunas: [
                {name:"id",field:"id",label:"Id",sortable:true,align:'center'},
                {name:"campo2",field:"campo1",label:"Campo1",sortable:true,align:'center'},
                {name:"campo3",field:"campo2",label:"Campo2"}, //,tipo:'Boolean'
                                                                       'Datetime'
                                                                       'Date'
                                                                       'Time'
                                                                       'Idade',campo:'nome do campo'
                                                                       'Array',campo:'nome do campo' -> se não for passado o campo pega-se o item inteiro
                                                                       'Object',campo:'nome do campo'
                                                                       'Numerico',decimal:'quantidade de numeros apos a virgula'
                                                                       'Concat',campo:'nome do segundo campo',prefixo:'texto antes do segundo campo',sufixo:'texto depois do segundo campo' -> se vai concatenar com algum outro campo (pode-se usar o teste)
                                                                 ,teste:{campo:'campo a ser testado',valor:'valor a ser testado'}
                {name:"acoes",field:"acoes",label:"Ações",align:'right'} //cria as ações de visualizar/editar/excluir
            ],
            tabelaPaginacao: {
                sortBy: 'campo1',
                descending: false,
                page: 1,
                rowsNumber: 0,
                rowsPerPage: 10 //10,25,50,100
            },
            imprimirColunas: [
                {name:"id",field:"id",label:"Id",sortable:true,align:'center'},
                {name:"campo2",field:"campo1",label:"Campo1",sortable:true,align:'center'}
            ]
        }
    }
}
```

| Funções | Parâmetros |
| ------ | ------ |
| :titulo | String título da tabela |
| :tabelaDados | função do crud) |
| :tabelaColunas | Array |
| :tabelaPaginacao | Objeto |
| :linhasPorPagina | Array (opcional) |
| :tabelaLoading | função do crud |
| imprimirColunas | objecto para ser utilizado no navbar (imprimir) |
| :bSelecionarDado | true/false (utilizado em conjunto com o array tabelaColunas -> acoes) |
| :bShow | true/false (utilizado em conjunto com o array tabelaColunas -> acoes) |
| :bEditar | true/false (utilizado em conjunto com o array tabelaColunas -> acoes) |
| :bExcluir | true/false (utilizado em conjunto com o array tabelaColunas -> acoes) |

## Filho
> Componente utilizado como `import` que contém a tabela detalhe dos dados
```
<filho titulo="Itens" :paiId="dado.id" foreignKey="dadoPaiId" :tabelaColunas="tabelaColunasFilho" :tabelaColunasVisiveis="tabelaColunasVisiveis" storeState="storeItens" entidadeApi="entidade"/>

export default {
    data() {
        return {
            tabelaColunasFilho: [
                {name:"paiId",field:"paiId",label:"paiId",campoVisivel:false},
                {name:"id",field:"id",label:"Id",campoVisivel:false},
                {name:"campo1",field:"campo1",label:"Campo1",align:'center',campoVisivel:true,tamanho:4},
                {name:"campo2",field:"campo2",label:"Campo2",align:'center',campoVisivel:true,tamanho:6},
                {name:"acoes",field:"acoes",label:"Ações",align:'right',campoVisivel:false}
            ],
            tabelaColunasVisiveis: ['id','campo1','campo2','acoes']
        }
    }
}
```

| Funções | Parâmetros |
| ------ | ------ |
| titulo | String título da tabela filho |
| :paiId | Objeto com o id do pai |
| foreignKey | String com o nome do campo para vincular com o paiId |
| :tabelaColunas | Array (Objecto campoVisivel = true mostra o campo para criação e/ou edição com o tamanho informado) |
| :tabelaColunasVisiveis | Array com as colunas visíveis da tabela |
| storeState | nome para o store |
| entidadeApi | nome da entidade da chamada loopback |
| :bCriar | true/false |
| :bEditar | true/false (utilizado em conjunto com o array tabelaColunas -> acoes) |
| :bExcluir | true/false (utilizado em conjunto com o array tabelaColunas -> acoes) |

>Função `async validarFilho()` que pode ser implementada. Será sempre chamada antes de gravar os dados. Implementar a função `limparValidarFilho()` para limpar as variáveis.
```
<filho @validarFilho="validarFilho" @limparValidarFilho="limparValidarFilho" titulo="Itens" :paiId="dado.id" foreignKey="dadoPaiId" :tabelaColunas="tabelaColunasFilho" :tabelaColunasVisiveis="tabelaColunasVisiveis" storeState="storeItens" entidadeApi="entidade"/>
            
export default {
    data() {
        return {
            validacaoFilho: true
        }
    },
    methods: {
        async validarFilho(df,cb) {
            this.validacaoFilho = false
            cb(this.validacaoFilho)
        },
        limparValidarFilho() {
            this.validacaoFilho = true
        }
    }
}
```

| Slots |  |
| ------ | ------ |
| mais-botao | insere ao lado dos botoes atuais |

> Pode-se alterar os `actions` padrões do Store do Vue

| Funções | Parâmetros |
| ------ | ------ |
| storeCarregarLista | novo nome ('carregarListaCrudPadrao') |
| storeCarregar | novo nome ('carregarCrudPadrao') |
| storeSalvar | novo nome ('salvarCrudPadrao') |
| storeExcluir | novo nome ('excluirCrudPadrao') |

## Galeria
> Componente utilizado como `import` que contém a galeria dos arquivos
```
<galeria :lista="arquivos" ref="galeria"/>

this.$refs.galeria.abrirGaleria(idx)
```

## Imprimir
> Componente utilizado como `import` que contém a tabela para impressão
```
<imprimir/>
```

## Utils
> Componente utilizado como `import` que contém a funções
```
export default {
    mixins: [utils]
}
```

| Funções |  |
| ------ | ------ |
| mostrarNotificacao | mostra notificação |
| mostrarMsgErro | mostra mensagens de erro |
| formatarDataHora | formata a data para DD/MM/YYYY HH:mm:ss |
| formatarData | formata a data para DD/MM/YYYY |
| formatarHora | formata a data para HH:mm:ss |
| formatarDataHoraArquivo | formata a data para YYYYMMDD_HHmmss |
| formatarArray | formata o array em string |
| formatarObject | formata o object em string |
| formatarIdade | formata a idade em string |
| testarLogin | testa o login |
| arrayToStringPorCampo | formata todos os campos do array em string |
| isNull | testa se é nulo |
| setTituloPagina | seta o título |
| mensagemValidacao | mostra mensagem de validação |
| abrirRota | abre a rota |
| abrirUrl | abre a url |
| getNewObjectId | obtem novo id no padrão mongo |
| converterDadoSemTipoObject | converte o item 'dado' retirando os tipos Object |